const state = {
    API_KEY: null,
    AUTH_DOMAIN: null,
    DATABASE_URL: null,
    PROJECT_ID: null,
    STORAGE_BUCKET: null,
    MESSAGINGSENDER_ID: null,
    APP_ID: null,
    MEASUREMENT_ID: null,

    FIREBASE_API_KEY: "AIzaSyBg805CGMMY5IgHo_WK1wA7GBAaoGEuLjw",
    FIREBASE_AUTH_DOMAIN: "fir-analytics-fb796.firebaseapp.com",
    FIREBASE_DATABASE_URL: "https://fir-analytics-fb796.firebaseio.com",
    FIREBASE_PROJECT_ID: "fir-analytics-fb796",
    FIREBASE_STORAGE_BUCKET: "fir-analytics-fb796.appspot.com",
    FIREBASE_MESSAGINGSENDER_ID: "746983484153",
    FIREBASE_APP_ID: "1:746983484153:web:36c902e62ad7a1e6e81028",
    FIREBASE_MEASUREMENT_ID: "G-3Y5K5DHX4N"
}

const getters = {
    API_KEY: (state) => {
        return state.API_KEY
    },
    AUTH_DOMAIN: (state) => {
        return state.AUTH_DOMAIN
    },
    DATABASE_URL: (state) => {
        return state.DATABASE_URL
    },
    PROJECT_ID: (state) => {
        return state.PROJECT_ID
    },
    STORAGE_BUCKET: (state) => {
        return state.STORAGE_BUCKET
    },
    MESSAGINGSENDER_ID: () => {
        return state.MESSAGINGSENDER_ID
    },
    APP_ID: (state) => {
        return state.APP_ID
    },
    MEASUREMENT_ID: (state) => {
        return state.MEASUREMENT_ID
    }
}

const mutations = {
  firebase_constants: (state) => {
      state.API_KEY = state.FIREBASE_API_KEY,
      state.AUTH_DOMAIN = state.FIREBASE_AUTH_DOMAIN,
      state.DATABASE_URL = state.FIREBASE_DATABASE_URL,
      state.PROJECT_ID = state.FIREBASE_PROJECT_ID,
      state.STORAGE_BUCKET = state.FIREBASE_STORAGE_BUCKET,
      state.MESSAGINGSENDER_ID = state.FIREBASE_MESSAGINGSENDER_ID,
      state.APP_ID = state.FIREBASE_APP_ID,
      state.MEASUREMENT_ID = state.FIREBASE_MEASUREMENT_ID
  }
}

const actions = {
    configure_firebase_constants: (context) => {
        context.commit('firebase_constants')
    }
}

export const config = { state, getters, mutations, actions }