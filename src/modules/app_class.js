export { appClass }

import Vue from 'vue'
import App from '../App'
import store from '../store/index.js'

class appClass {
    constructor () {
        
        import('./config_class').then((config) => {
            this.config = new config.configClass()
            this.loadVue()
        })

    }

    loadVue () {
        new Vue({
            el: '#app',
            store,
            components: { App },
            template: '<App/>'
        })
    }
}