export { configClass }

import store from '../store'
import firebase from 'firebase/app'

class configClass {
    constructor () {
        this.loadConfig()
    }
    loadConfig () {

        if (firebase.apps.length) return

        store.dispatch('configure_firebase_constants')

        let config = {
            apiKey: store.getters.API_KEY,
            authDomain: store.getters.AUTH_DOMAIN,
            databaseURL: store.getters.DATABASE_URL,
            projectId: store.getters.PROJECT_ID,
            storageBucket: store.getters.STORAGE_BUCKET,
            messagingSenderId: store.getters.MESSAGINGSENDER_ID,
            appId: store.getters.APP_ID,
            measurementId: store.getters.MEASUREMENT_ID
        }

        firebase.initializeApp(config)
        console.log(firebase)
    }
}